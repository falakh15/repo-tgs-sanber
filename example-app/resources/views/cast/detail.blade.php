@extends('layout.master')
@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('content')

<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<br><br>
<a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
<a href="/cast" class="btn btn-primary btn-sm">Back</a>
@endsection