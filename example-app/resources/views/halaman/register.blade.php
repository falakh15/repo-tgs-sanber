@extends('layout.master')
@section('judul')
SanberBook
@endsection
    
@section('content')
    
    <h1>Buat Account Baru !</h1>
    <h2>Sign Up Form</h2>
  <form action="/kirim" method="POST">
    @csrf
    <p>First Name : <input type="text" name="firstname" required></p>
    <p>Last Name : <input type="text" name="lastname" required></p>
    <p>Gender :</p>
    
        <input type="radio" value="Male" name="gender" required> Male<br>
        <input type="radio" value="Female" name="gender" required> Female<br>
        <input type="radio" value="Other" name="gender" required> Other<br>
    <p>Nationality :</p>
        <select name="Nationality">
            <option value="-">Belum Memilih</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Zimbabwe">Zimbabwe</option>
            <option value="China">China</option>
        </select>
    <p>Language Speak :</p>
    <input type="checkbox" name="lang" value="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="lang" value="English">English<br>
    <input type="checkbox" name="lang" value="Other">Other<br>
    <p>Bio :</p>
    <textarea  name="bio" cols="25" rows="5"></textarea><br>
    
        <input type="submit" name="signup" value="Sign Up">
    </form>
    
    @endsection
