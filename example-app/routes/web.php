<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);


Route::get('/biodata', [AuthController::class, 'register']);
Route::post('/kirim', [AuthController::class, 'kirim']);
Route::get('/data', function(){
    return view('halaman.data');

});
Route::get('/data-table', function(){
    return view('halaman.datatable');

});


//CRUD cast
//CREATE
Route::get('/cast/create',[CastController::class,'create']);
//kirim data db
Route::post('/cast',[CastController::class,'store']);

//READ
//tampil
Route::get('/cast',[CastController::class,'index']);
//detail read berdasar cast
Route::get('/cast/{cast_id}',[CastController::class,'show']);

//UPDATE
//form update
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit']);
Route::put('/cast/{cast_id}',[CastController::class,'update']);

//DELETE
Route::delete('/cast/{cast_id}',[CastController::class,'destroy']);
