<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }
    public function kirim(Request $request){
        $firstname=$request['firstname'];
        $lastname=$request['lastname'];
        $gender=$request['gender'];
        $nationality=$request['nationality'];
        $lang=$request['lang'];
        $bio=$request['bio'];
        return view('halaman.home',[
            'firstname'=>$firstname,
            'lastname'=>$lastname,
            'gender'=>$gender,
            'nationality'=>$nationality,
            'lang'=>$lang,
            'bio'=>$bio
        ]);
    }
}
