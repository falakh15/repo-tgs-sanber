<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");

echo "Nama Hewan = ".$sheep->name."<br>"; // "shaun"
echo "Jml Kaki = ".$sheep->legs."<br>"; // 4
echo "Berdarah Dingin = ".$sheep->cold_blooded."<br>"; // "no"
echo "<br>";
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
$kodok = new Frog("buduk");
echo "Nama Hewan = ".$kodok->name."<br>"; // "shaun"
echo "Jml Kaki = ".$kodok->legs."<br>";
echo "Berdarah Dingin = ".$kodok->cold_blooded."<br>"; // "no";
$kodok->jump() ; // "hop hop"
echo "<br><br>";
$sungokong = new Ape("kera sakti");
echo "Nama Hewan = ".$sungokong->name."<br>"; // "shaun"
echo "Jml Kaki = ".$sungokong->legs."<br>";
echo "Berdarah Dingin = ".$sungokong->cold_blooded."<br>"; // "no"
$sungokong->yell(); // "Auooo"
echo "<br><br>";

?>